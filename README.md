# Crypt Raider - Unreal Engine

Designed a dungeon and crypt level which the player can explore.

There are puzzles to solve

Things to steal

### Dungeon Area

![](images/CryptRaiderP1.gif)


### Crypt Area

![](images/CryptRaiderP2.gif)

## How to install?
1. Download the Windows directory 
2. Extract contents into a folder
3. Open up the Windows/CryptRaider.exe

## How to play?
Move forward, backwards, left, and right with W (forward),  S (backwards), A (left) and D (right) keys.

Use SpaceBar to jump
